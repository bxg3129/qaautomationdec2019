package com.cerotid.inheritance;

interface CarWash {
	double getFullService();

	double getBasicService();

	double getFreeVaccuum();
}

package com.cerotid.inheritance;

public class Ford150 extends Truck implements CarWash {

	@Override
	String getGasolineType() {
		// TODO Auto-generated method stub
		return "Gas";
	}

	@Override
	public double getFullService() {
		// TODO Auto-generated method stub
		double cost = 29.99;
		System.out.println(this.getClass() +"received full service of value: "+ cost);
		
		return cost;
	}

	@Override
	public double getBasicService() {
		// TODO Auto-generated method stub
		double cost = 9.99;
		
		System.out.println(this.getClass() +"received basic service of value: "+ cost);
		
		return cost;
	}

	@Override
	public double getFreeVaccuum() {
		double cost = 0.00;
		
		System.out.println(this.getClass() +"received free vacuum service of value: "+ cost);
		
		return cost;
	}

}

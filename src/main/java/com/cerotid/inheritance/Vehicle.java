package com.cerotid.inheritance;

public abstract class Vehicle {
	private String make;
	private int year;
	private String model;
	private double mileage;
	private String type;
	
	public Vehicle() {
		
	}
	
	public Vehicle(String make, int year, String model, double mileage, String type) {
		super();
		this.make = make;
		this.year = year;
		this.model = model;
		this.mileage = mileage;
		this.type = type;
	}

	protected double maxSpeed() {
		double maxSpeed;
		
		if(this.year > 2016) {
			maxSpeed =  150.00;
		}
		else
			maxSpeed = 120.00;
		
		return maxSpeed;
	}
	
	protected int numberOfDoors() {
		return 4;
	}
	
	@Override
	public String toString() {
		return "Vehicle [make=" + make + ", year=" + year + ", model=" + model + ", mileage=" + mileage + ", type="
				+ type + "]";
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public double getMileage() {
		return mileage;
	}

	public void setMileage(double mileage) {
		this.mileage = mileage;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	
}

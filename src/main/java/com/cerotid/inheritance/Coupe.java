package com.cerotid.inheritance;

public class Coupe extends Vehicle {
	private String x;
	
	public Coupe() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Coupe(String make, int year, String model, double mileage, String type, String x) {
		super(make, year, model, mileage, type);
		// TODO Auto-generated constructor stub
		this.x =x;
	}

	@Override
	protected int numberOfDoors() {
		// TODO Auto-generated method stub
		return 2;
	}
}

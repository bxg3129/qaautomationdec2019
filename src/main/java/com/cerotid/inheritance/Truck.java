package com.cerotid.inheritance;

public abstract class Truck extends Vehicle {
	abstract String getGasolineType();
}

package com.cerotid.core;

public class LoopConcept {
	// for loop
	// while loop
	// do .. while

	public static void main(String[] args) {
		// Create an instance of a class to utilize its non-static methods
		new LoopConcept();
		LoopConcept lc = new LoopConcept();

		// Now we are ready to invoke LoopConcept class non-static behaviors/methods
		lc.showMeHowForLoopWorks();

		lc.showMeHowWhileLoppWorks();

		lc.showMeHowDoWhileWorks();
	}

	void showMeHowForLoopWorks() {
		// TODO For loop logic here
		System.out.println("About to learn For Loop Concept");

		// print 1-10 using for loop
		for (int i = 1; i < 11; i++) {
			System.out.print(i);

			System.out.print(" ");
		}

		System.out.print("\n");
	}

	void showMeHowWhileLoppWorks() {
		// TODO while Loop logic here
		System.out.println("About to learn While Loop Concept");

		// print 1-10 using while loop
		int i = 1;

		while (i < 11) {
			System.out.print(i);

			System.out.print(" ");

			i++;
		}

		System.out.print("\n");
	}

	void showMeHowDoWhileWorks() {
		// TODO Do while Loop logic here
		System.out.println("About to learn Do..while Concept");

		int i = 1;

		do {
			System.out.print(i);

			System.out.print(" ");

			i++;

		} while (i < 11);
		
		System.out.print("\n");
	}

}

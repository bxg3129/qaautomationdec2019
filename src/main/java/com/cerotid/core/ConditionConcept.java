package com.cerotid.core;

public class ConditionConcept {
	public static void main(String[] args) {
		//if condition 
		//switch cases
		ConditionConcept conditionConcept = new ConditionConcept();
		conditionConcept.showMeHowIfConditionWorks();
		conditionConcept.showMeHowSwitchCasesnWorks();
	}

	private void showMeHowIfConditionWorks() {
		// TODO Auto-generated method stub
		System.out.println("About to see how IF condition works");
		
		UserInputConcept uc = new UserInputConcept();
		int age = uc.getUsersAge();
		
		//if..else
		if(age >= 21) {
			System.out.println("Alcohol SOLD.");
		}
		else {
			System.out.println("Alcohol CANNOT be purchased");
		}
		
		//if.. else if ..else
		if(age< 18) {
			System.out.println("I can't seel you neither lottery nor ALCOHOL");
		}
		else if (age >= 18 && age < 21) {
			System.out.println("I can you lottery but no ALCOHOL");
		}
		else
			System.out.println("I can sell you both Alcohol & Lottery");
		
	}

	private void showMeHowSwitchCasesnWorks() {
		// TODO Auto-generated method stub
		System.out.println("About to see how switch Case works");
		
		UserInputConcept uc = new UserInputConcept();
		
		String key = uc.getUserChoiceColorCode() ;
		
		switch (key) {
		case "R":
			System.out.println("You chose color RED");
			break;
		case "G":
			System.out.println("You chose color GREEN");
			break;	
		case "B":
			System.out.println("You chose color BLUE");
			break;	
			
		default:
			System.out.println("You pick the random color");
			break;
		}
		
	}
}

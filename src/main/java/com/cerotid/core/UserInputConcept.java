package com.cerotid.core;

import java.util.Scanner;

public class UserInputConcept {
	public static void main(String[] args) {
		UserInputConcept uc = new UserInputConcept();
		
		uc.showMeHowToTakeUserInput();
		
		System.out.println("Your age is: "+ uc.getUsersAge());
	}

	private void showMeHowToTakeUserInput() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please provide your Name");
		String name = sc.next();
		
		System.out.println("User name is: "+ name );
		
	}
	
	public int getUsersAge() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please provide your age");
		int age = sc.nextInt();
		
		return age;
	}
	
	public String getUserChoiceColorCode() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please provide your one character color code");
		String colorCode = sc.next();
		
		return colorCode;
	}
}

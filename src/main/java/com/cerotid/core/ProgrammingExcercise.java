package com.cerotid.core;

public class ProgrammingExcercise {
	public boolean checkIfNumberIsOddAndDivisibleByFive(int number) {
		if(isNumberOdd(number)  && isNumberDivisibleByFive(number)) {
			return true;
		}
		
		return false;
		
	}

	protected boolean isNumberOdd(int number) {
		// TODO Auto-generated method stub
		
		if(Math.abs(number) % 2 == 1)
			return true;
		
		return false;
	}

	protected boolean isNumberDivisibleByFive(int number) {
		// TODO Auto-generated method stub
		if(Math.abs(number) % 5 == 0)
			return true;
		
		return false;
	}
}

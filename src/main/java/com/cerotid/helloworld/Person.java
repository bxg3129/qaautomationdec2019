package com.cerotid.helloworld;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Person {
	//member Variable
	//field name
	//properties
	//instance variable
	private String name;
	private int age;
	private char sex;
	private boolean isLiving = true;
	private double salary;
	private String ssn;
	
	//class variable
	
	public Person() {
		
	}
	
	public Person(String name, String ssn) {
		super();
		this.name = name;
		
		if(isSSNValid(ssn))
			this.ssn = ssn;
	}
	
	public Person(String name, String ssn, double salary) {
		super();
		this.name = name;
		this.salary = salary;
		
		if(isSSNValid(ssn))
			this.ssn = ssn;
	}

	
	protected boolean isSSNValid(String ssn2) {
		String regex = "^(?!000|666)[0-8][0-9]{2}-(?!00)[0-9]{2}-(?!0000)[0-9]{4}$";
		 
		Pattern pattern = Pattern.compile(regex);
		
		 Matcher matcher = pattern.matcher(ssn2);
		 
		// TODO Auto-generated method stub
		return matcher.matches();
	}

	//primitives & Reference Type Variables
	public static void main(String[] args) {
		//instantiation
		Person person1 = new Person();
		person1.name = "David";
		person1.ssn = "select % from employee";
		
	
		System.out.println(person1.name);
		
		person1.speak();
		
		Person person2 = new Person();
		person2.name = "Sam";
		
		person2.speak();
		
		
	}
	
	
	void speak(){
		System.out.println("My name is:"+ name);
	}
	
	void taste(String food) {
		
	}
	
	double run(int milesRun){
		return milesRun* 0.01;
	}
	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public char getSex() {
		return sex;
	}

	public void setSex(char sex) {
		this.sex = sex;
	}

	public boolean isLiving() {
		return isLiving;
	}

	public void setLiving(boolean isLiving) {
		this.isLiving = isLiving;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		if(isSSNValid(ssn))
			this.ssn = ssn;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", sex=" + sex + ", isLiving=" + isLiving + ", salary="
				+ salary + ", ssn=" + ssn + "]";
	}
	
	

}

package com.cerotid.ooconcept;

public class InheritanceConcept {
	//General class
	//Child (Specialized) class extends General class
	//Child  inherits properties and behaviour from parent
	//overriding
	//keyword: extends; implements; super
	// childClass extends ParentClass
		// childClass can only extend one ParentClass
	//childClass implements ParentInterface
		//childClass can implements more than one Parent Interface
}

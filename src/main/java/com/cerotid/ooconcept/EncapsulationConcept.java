package com.cerotid.ooconcept;

import com.cerotid.helloworld.Person;

public class EncapsulationConcept {
	public static void main(String[] args) {
		Person person = new Person("David", "123-10-1754");
		person.setSsn("123-10-1745");
		
		System.out.println(person);
		
		Person person2 = new Person("David", "123-10-1754");
		
		System.out.println(person2);
		
		Person person3 = new Person("Sam", "600-12-1453", 100000.00);
		
		System.out.println(person3);
		
		Person person4 = new Person();
		person4.setName("Kim");
		person4.setSsn("1234523");
		
		System.out.println(person4);
		
		System.out.println("Local Branching is messed up");
	}

}

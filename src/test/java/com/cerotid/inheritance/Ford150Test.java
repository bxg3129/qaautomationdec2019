package com.cerotid.inheritance;

import static org.junit.Assert.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

public class Ford150Test {
	Ford150 ford150;

	@Before
	public void setUp() throws Exception {
		ford150 = new Ford150();
	}

	@Test
	public void testGetGasolineType() {
		//Expected vs Actual??
		//"Gas" vs ford150.getGasolineType()
		assertEquals("Gas", ford150.getGasolineType());
	}
	
	@Test
	public void negativeTestGetGasolineType() {
		//Expected vs Actual??
		//"Gas" vs ford150.getGasolineType()
		assertNotEquals("123", ford150.getGasolineType());
		assertNotEquals("xyza", ford150.getGasolineType());
		assertNotEquals("", ford150.getGasolineType());
		assertNotEquals("GAS", ford150.getGasolineType());
	}

	@Test
	public void testGetFullService() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetBasicService() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetFreeVaccuum() {
		fail("Not yet implemented");
	}

}

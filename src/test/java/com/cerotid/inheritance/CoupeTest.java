package com.cerotid.inheritance;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CoupeTest extends VehicleTest {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}
	
	@Override
	protected void createVehicleObject() {
		super.vehicle = new Coupe();
		super.vehicle2 = new Coupe();
	}

	@Test
	public void testNumberOfDoors() {
		assertEquals(2, super.vehicle.numberOfDoors());
	}

	@Test
	public void testMaxSpeed() {
		fail("Not yet implemented");
	}

}

package com.cerotid.inheritance;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VehicleTest {
	protected Vehicle vehicle;
	protected Vehicle vehicle2;

	@Before
	public void setUp() throws Exception {
		createVehicleObject();
	}

	protected void createVehicleObject() {
		// TODO Auto-generated method stub
		vehicle = new Ford150();
		vehicle.setMake("Honda");
		vehicle.setModel("Accord");
		vehicle.setMileage(20001.3);
		vehicle.setType("Sedan");
		vehicle.setYear(2019);
		
		vehicle2 = new Ford150();
		//vehicle2 = new Vehicle();
		vehicle2.setMake("Honda");
		vehicle2.setModel("Accord");
		vehicle2.setMileage(10001.3);
		vehicle2.setType("Sedan");
		vehicle2.setYear(2010);
	}

	@Test
	public void testMaxSpeedForVehicleYearGreaterThan2016() {
		assertEquals(150.00, vehicle.maxSpeed(),0.01);
	}
	
	@Test
	public void testMaxSpeedForVehicleYearLessThanOrEqualTo2016() {
		assertEquals(120.00, vehicle2.maxSpeed(),0.01);
	}

	@Test
	public void testNumberOfDoors() {
		assertEquals(4, vehicle.numberOfDoors());
	}

	@Test
	public void testToString() {
		assertEquals("Vehicle [make=Honda, year=2019, model=Accord, mileage=20001.3, type=Sedan]", vehicle.toString());
	}

}

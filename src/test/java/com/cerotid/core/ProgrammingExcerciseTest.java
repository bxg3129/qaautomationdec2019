package com.cerotid.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ProgrammingExcerciseTest {
	ProgrammingExcercise programingExcersize;

	@Before
	public void setUp() throws Exception {
		programingExcersize = new ProgrammingExcercise();
	}

	@Test
	public void testCheckIfNumberIsOddAndDivisibleByFive() {
		assertTrue(programingExcersize.checkIfNumberIsOddAndDivisibleByFive(25));
		assertFalse(programingExcersize.checkIfNumberIsOddAndDivisibleByFive(30));
	}
	
	@Test
	public void testIsNumberOdd() {
		assertTrue(programingExcersize.isNumberOdd(25));
		assertFalse(programingExcersize.isNumberOdd(24));
		assertFalse(programingExcersize.isNumberOdd(-24));
		assertTrue(programingExcersize.isNumberOdd(-13));
	}
	
	@Test
	public void testIsNumberDivisibleByFive() {
		assertTrue(programingExcersize.isNumberDivisibleByFive(10));
		assertFalse(programingExcersize.isNumberDivisibleByFive(12));
		assertTrue(programingExcersize.isNumberDivisibleByFive(-10));
		assertFalse(programingExcersize.isNumberDivisibleByFive(-12));
	}

}
